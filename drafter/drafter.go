package drafter

import (
	"context"
	_ "embed"
	"fmt"
	"log"

	"github.com/tetratelabs/wazero"
	"github.com/tetratelabs/wazero/api"
	"github.com/tetratelabs/wazero/imports/wasi_snapshot_preview1"
)

//go:embed drafter.wasm
var drafterWasm []byte

type drafter struct {
	w wazero.Runtime
	m api.Module
}

func newDrafter(ctx context.Context) (*drafter, error) {
	w := wazero.NewRuntime(ctx)

	_, err := w.NewHostModuleBuilder("env").
		NewFunctionBuilder().WithFunc(logString).Export("log").
		NewFunctionBuilder().WithFunc(func(context.Context, uint32) {}).Export("emscripten_notify_memory_growth").
		Instantiate(ctx)

	_, err = wasi_snapshot_preview1.Instantiate(ctx, w)
	if err != nil {
		return nil, fmt.Errorf("instantiating wasi: %w", err)
	}

	mod, err := w.Instantiate(ctx, drafterWasm)
	if err != nil {
		return nil, fmt.Errorf("instantiate wasm: %w", err)
	}

	return &drafter{
		w: w,
		m: mod,
	}, nil
}

type ParseOptions struct {
	GenerateSourceMap         bool
	ExportSourceMap           bool
	RequireBlueprintName      bool
	GenerateMessageBody       bool
	GenerateMessageBodySchema bool
}

func (d *drafter) parse(ctx context.Context, blueprint []byte, options ParseOptions) error {
	fn := d.m.ExportedFunction("c_serialize_json_options")
	if fn == nil {
		return fmt.Errorf("func not found")
	}
	xs, err := fn.Call(ctx)
	if err != nil {
		return err
	}
	log.Println(xs)
	return nil
}

func logString(_ context.Context, m api.Module, offset, byteCount uint32) {
	buf, ok := m.Memory().Read(offset, byteCount)
	if !ok {
		log.Panicf("Memory.Read(%d, %d) out of range", offset, byteCount)
	}
	fmt.Println(string(buf))
}

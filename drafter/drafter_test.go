package drafter_test

import (
	"context"
	"testing"

	"anime.bike/grafter/drafter"
	"github.com/stretchr/testify/require"
)

func TestParseSimple(t *testing.T) {

	ctx := context.Background()
	err := drafter.Parse(ctx, nil, drafter.ParseOptions{})
	require.NoError(t, err)
}

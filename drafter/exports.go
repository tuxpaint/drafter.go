package drafter

import "context"

var global *drafter

func init() {
	var err error
	global, err = newDrafter(context.Background())
	if err != nil {
		panic(err)
	}
}

func Parse(ctx context.Context, blueprint []byte, options ParseOptions) error {
	return global.parse(ctx, blueprint, options)
}

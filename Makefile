

.PHONY: clean deps target install

DRAFTER=_drafter
#BUILDFLAGS=-Oz
#FLAGS=-Oz --llvm-lto 1
DRAFTER_EXPORTED_FUNCTIONS=['_drafter_init_parse_options', '_drafter_free_parse_options', '_drafter_set_name_required', '_drafter_set_skip_gen_bodies', '_drafter_set_skip_gen_body_schemas', '_drafter_init_serialize_options', '_drafter_free_serialize_options', '_drafter_set_sourcemaps_included', '_drafter_set_format', '_drafter_free_result', '_c_serialize_json_options', '_c_buffer_ptr', '_c_buffer_string', '_c_free_buffer_ptr', '_c_parse_to', '_c_validate_to']


target: tmp/drafter.wasm

clean:
	cd ${DRAFTER} && make clean
	rm -rf build


${DRAFTER}/config.gypi:
	cd ${DRAFTER} && emconfigure python2 ./configure

deps: ${DRAFTER}/build/out/Release/libdrafter.a
${DRAFTER}/build/out/Release/libdrafter.a: ${DRAFTER}/config.gypi
	cd ${DRAFTER} && CFLAGS=${BUILDFLAGS} CXXFLAGS=${BUILDFLAGS} emmake make -j 4 test-libdrafter

build: CMakeLists.txt
	mkdir -p build
	cd build && emcmake cmake ..
	rm -rf tmp/*

build/libdraftershim.a: _shim/draftershim.cc _shim/draftershim.h build
	cd build && emmake make

install: tmp/drafter.wasm
	cp tmp/drafter.wasm drafter/drafter.wasm

tmp/drafter.wasm: ${DRAFTER}/build/out/Release/libdrafter.a build/libdraftershim.a
	mkdir -p tmp
	em++ ${FLAG} --no-entry "build/libdraftershim.a" \
        "${DRAFTER}/build/out/Release/libapib.a" \
        "${DRAFTER}/build/out/Release/libsundown.a" \
        "${DRAFTER}/build/out/Release/libapib-parser.a" \
        "${DRAFTER}/build/out/Release/libdrafter.a" \
        -s EXPORTED_FUNCTIONS="${DRAFTER_EXPORTED_FUNCTIONS}" \
        -s DISABLE_EXCEPTION_CATCHING=1 \
        -s EXPORTED_RUNTIME_METHODS="['stringToUTF8', 'getValue', 'Pointer_stringify', 'lengthBytesUTF8', 'UTF8ToString']" \
        -s ASSERTIONS=0 \
        -s DOUBLE_MODE=0 \
        -s ALLOW_MEMORY_GROWTH=1 \
        -s INVOKE_RUN=1 \
        -s NO_FILESYSTEM=1 \
        -s ELIMINATE_DUPLICATE_FUNCTIONS=1 \
        -s AGGRESSIVE_VARIABLE_ELIMINATION=1 \
        -o tmp/drafter.wasm
